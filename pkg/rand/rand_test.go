package rand

import (
	"testing"
	"unicode/utf8"

	"github.com/stretchr/testify/require"
)

func TestAlphabet(t *testing.T) {
	expected := []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
	require.Equal(t, expected, alphabet)
}

func TestString(t *testing.T) {
	const size = 24

	availableRunes := make(map[rune]bool, len(alphabet))
	for _, ch := range alphabet {
		availableRunes[ch] = true
	}

	for i := 0; i < 10; i++ {
		result, err := String(size)
		require.NoError(t, err)
		require.Equal(t, size, utf8.RuneCountInString(result))
		for _, ch := range result {
			if !availableRunes[ch] {
				t.Errorf("Found unavailable rune %s (%d) in string %q", string(ch), ch, result)
			}
		}
	}
}
