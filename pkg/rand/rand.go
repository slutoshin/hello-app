package rand

import (
	"crypto/rand"
	"fmt"
	"strings"
)

var alphabet []rune

// nolint:gochecknoinits
func init() {
	for ch := 'a'; ch <= 'z'; ch++ {
		alphabet = append(alphabet, ch)
	}

	for ch := 'A'; ch <= 'Z'; ch++ {
		alphabet = append(alphabet, ch)
	}

	for ch := '0'; ch <= '9'; ch++ {
		alphabet = append(alphabet, ch)
	}

	b := make([]byte, 1)
	if _, err := rand.Read(b); err != nil {
		panic(fmt.Errorf("rand.Rand is unavailable: %s", err))
	}
}

func String(size int) (string, error) {
	randBytes := make([]byte, size)
	_, err := rand.Read(randBytes)
	if err != nil {
		return "", err
	}

	b := &strings.Builder{}
	b.Grow(size)
	for i := range randBytes {
		_, _ = b.WriteRune(alphabet[int(randBytes[i])%len(alphabet)]) // errors is always nil
	}

	return b.String(), nil
}
