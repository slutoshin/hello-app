package main

import (
	"context"
	"database/sql"
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/vadimlarionov/hello-app/internal/services"
	"gitlab.com/vadimlarionov/hello-app/internal/users"
	"gitlab.com/vadimlarionov/hello-app/pkg/rand"
	"go.uber.org/zap"
)

const (
	sessionIDLength   = 24
	sessionCookieName = "x-session-id"
)

type Handler struct {
	logger         *zap.SugaredLogger
	helloClient    services.HelloServiceClient
	sessionStorage users.SessionStorage
}

func NewHandler(logger *zap.Logger, helloClient services.HelloServiceClient, sessionStorage users.SessionStorage) *Handler {
	return &Handler{
		logger:         logger.Sugar(),
		helloClient:    helloClient,
		sessionStorage: sessionStorage,
	}
}

func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()
	r.Get("/", h.Hello)
	return r
}

func (h *Handler) Hello(w http.ResponseWriter, r *http.Request) {
	var accessToken string
	if authCookie, err := r.Cookie(sessionCookieName); err != nil {
		if err != http.ErrNoCookie {
			h.logger.Errorf("Can't find cookie by key %q: %s", sessionCookieName, err)
			writeInternalError(w)
			return
		}

		session, err := h.registerUser(r.Context(), w)
		if err != nil {
			h.logger.Errorf("Can't register user: %s", err)
			writeInternalError(w)
			return
		}
		accessToken = session.AccessToken
	} else {
		accessToken = authCookie.Value
	}

	session, err := h.sessionStorage.Find(accessToken)
	if err != nil {
		if errors.Cause(err) == sql.ErrNoRows {
			h.logger.Warnf("User uses custom session, reject it", err)
			http.SetCookie(w, &http.Cookie{
				Name:    sessionCookieName,
				Value:   "",
				Expires: time.Unix(0, 0),
			})
			w.WriteHeader(http.StatusForbidden)
			return
		}

		h.logger.Errorf("Can't find session in storage: %s", err)
		writeInternalError(w)
		return
	}

	helloRequest := services.HelloRequest{UserId: session.UserID}
	resp, err := h.helloClient.Hello(context.Background(), &helloRequest)
	if err != nil {
		h.logger.Warnf("Can't execute Hello rpc method by user %q: %s", helloRequest.UserId, err)
		writeInternalError(w)
		return
	}

	if _, err = w.Write(resp.Content); err != nil {
		h.logger.Warnf("Can't write response: %s", err)
	}
}

func (h *Handler) registerUser(ctx context.Context, w http.ResponseWriter) (*users.Session, error) {
	resp, err := h.helloClient.CreateUser(ctx, &services.Empty{})
	if err != nil {
		return nil, errors.Wrap(err, "can't create user")
	}

	accessToken, err := rand.String(sessionIDLength)
	if err != nil {
		return nil, errors.Wrap(err, "can't create random string")
	}

	session := &users.Session{
		AccessToken: accessToken,
		UserID:      resp.UserId,
	}

	if err = h.sessionStorage.Create(session); err != nil {
		return nil, errors.Wrapf(err, "can't create cession for user %d", session.UserID)
	}

	authCookie := &http.Cookie{
		Name:    sessionCookieName,
		Value:   session.AccessToken,
		Expires: time.Now().AddDate(1, 0, 0),
	}
	http.SetCookie(w, authCookie)

	return session, nil
}

func writeInternalError(w http.ResponseWriter) {
	http.Error(w, "500: internal server error", http.StatusInternalServerError)
}
