package users

import "time"

type User struct {
	ID         int64
	Name       string
	IsMale     bool
	CountViews string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

type Storage interface {
	Create(u *User) error
	Find(id int64) (*User, error)
	IncCountViews(id int64) error
}

type Session struct {
	AccessToken   string
	UserID        int64
	LastVisitedAt time.Time
	CreatedAt     time.Time
}

type SessionStorage interface {
	Create(session *Session) error
	Find(accessToken string) (*Session, error)
	UpdateLastVisitedAt(accessToken string) error
}
