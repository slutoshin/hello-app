package postgres

import (
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.com/vadimlarionov/hello-app/internal/users"
)

var _ users.SessionStorage = &SessionStorage{}

type SessionStorage struct {
	statementStorage

	createStmt          *sql.Stmt
	findStmt            *sql.Stmt
	updateLastVisitedAt *sql.Stmt
}

func NewSessionStorage(db *DB) (*SessionStorage, error) {
	s := &SessionStorage{statementStorage: newStatementsStorage(db)}

	stmts := []stmt{
		{Query: createSessionQuery, Dst: &s.createStmt},
		{Query: findSessionQuery, Dst: &s.findStmt},
		{Query: updateSessionLastVisitedAtQuery, Dst: &s.updateLastVisitedAt},
	}

	if err := s.initStatements(stmts); err != nil {
		return nil, errors.Wrap(err, "can't init statements")
	}

	return s, nil
}

const sessionFields = "access_token, user_id, last_visited_at, created_at"

func scanSession(scanner sqlScanner, s *users.Session) error {
	return scanner.Scan(&s.AccessToken, &s.UserID, &s.LastVisitedAt, &s.CreatedAt)
}

const createSessionQuery = `
INSERT INTO sessions(access_token, user_id, last_visited_at, created_at) 
VALUES ($1, $2, now(), now())
`

func (s *SessionStorage) Create(session *users.Session) error {
	_, err := s.createStmt.Exec(&session.AccessToken, &session.UserID)
	if err != nil {
		return errors.Wrap(err, "can't exec query")
	}

	return nil
}

const findSessionQuery = "SELECT " + sessionFields + " FROM sessions WHERE access_token=$1"

func (s *SessionStorage) Find(accessToken string) (*users.Session, error) {
	var session users.Session
	row := s.findStmt.QueryRow(accessToken)
	if err := scanSession(row, &session); err != nil {
		return nil, errors.Wrap(err, "can't scan session")
	}

	return &session, nil
}

const updateSessionLastVisitedAtQuery = "UPDATE sessions SET last_visited_at=now() WHERE access_token=$1"

func (s *SessionStorage) UpdateLastVisitedAt(accessToken string) error {
	if _, err := s.updateLastVisitedAt.Exec(accessToken); err != nil {
		return errors.Wrap(err, "can't exec query")
	}

	return nil
}
