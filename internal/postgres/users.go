package postgres

import (
	"database/sql"

	"github.com/pkg/errors"
	"gitlab.com/vadimlarionov/hello-app/internal/users"
)

var _ users.Storage = &UserStorage{}

type UserStorage struct {
	statementStorage

	createStmt          *sql.Stmt
	findStmt            *sql.Stmt
	incViewsCounterStmt *sql.Stmt
}

func NewUserStorage(db *DB) (*UserStorage, error) {
	s := &UserStorage{statementStorage: newStatementsStorage(db)}

	stmts := []stmt{
		{Query: createUserQuery, Dst: &s.createStmt},
		{Query: findUserQuery, Dst: &s.findStmt},
		{Query: incUserViewsCounterQuery, Dst: &s.incViewsCounterStmt},
	}

	if err := s.initStatements(stmts); err != nil {
		return nil, errors.Wrap(err, "can't init statements")
	}

	return s, nil
}

const userFields = "id, name, is_male, count_views, created_at, updated_at"

func scanUser(scanner sqlScanner, u *users.User) error {
	return scanner.Scan(&u.ID, &u.Name, &u.IsMale, &u.CountViews, &u.CreatedAt, &u.UpdatedAt)
}

const createUserQuery = "INSERT INTO users(name, is_male) VALUES ($1, $2) RETURNING id"

func (s *UserStorage) Create(u *users.User) error {
	if err := s.createStmt.QueryRow(u.Name, u.IsMale).Scan(&u.ID); err != nil {
		return errors.Wrap(err, "can't exec query")
	}

	return nil
}

const findUserQuery = "SELECT " + userFields + " FROM users WHERE id=$1"

func (s *UserStorage) Find(id int64) (*users.User, error) {
	var u users.User
	row := s.findStmt.QueryRow(id)
	if err := scanUser(row, &u); err != nil {
		return nil, errors.Wrap(err, "can't scan user")
	}

	return &u, nil
}

const incUserViewsCounterQuery = "UPDATE users SET count_views=count_views+1 WHERE id=$1"

func (s *UserStorage) IncCountViews(id int64) error {
	if _, err := s.incViewsCounterStmt.Exec(id); err != nil {
		return errors.Wrap(err, "can't exec query")
	}

	return nil
}
